package database;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.TreeItem;
import sysprint.PrintTeamData;


public class Database {
	
	private ArrayList<MatchObject> matchObjectList;
	private ArrayList<EntityMap> mapsList;
	private ArrayList<EntityCiv> civsList;
	private ArrayList<EntityPlayer> playersList;
	private ArrayList<String> playerCategory, civCategory, mapCategory;
	private final String db_file_name_prefix = "resources/database/AgeMaker.db";
	private Connection conn;
	private Statement statement;
	private ResultSet tableSet;
	private static Database dbUnique;
	private final String ressourcePath = "queries/queries.properties";
	private Properties prop;

	private Database() {
	}

	public static Database getInstanz() {
		if (dbUnique == null) {
			dbUnique = new Database();
			dbUnique.initDatabase();
		}
		return dbUnique;
	}

	private void initDatabase() {
		matchObjectList = new ArrayList<MatchObject>();
		playersList = new ArrayList<EntityPlayer>();
		mapsList = new ArrayList<EntityMap>();
		civsList = new ArrayList<EntityCiv>();

		try {
			setJDBC();
			loadQueryBundle();
			loadPlayer();
			loadMaps();
			loadCivs();
			Collections.sort(matchObjectList);
			statement.close();
			conn.close();
		} catch (SQLException ex) {
			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
			ex.printStackTrace();
		}
	}
	
	public void reloadDB(){
		playersList.clear();
		civsList.clear();
		mapsList.clear();
		try {
			setJDBC();
			loadPlayer();
			loadCivs();
			loadMaps();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void setJDBC() {
		try {
			String url = "jdbc:sqlite:" + db_file_name_prefix;
			conn = DriverManager.getConnection(url);
			statement = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void loadQueryBundle() {
		try {
			InputStream input = Database.class.getClassLoader().getResourceAsStream(ressourcePath);
			prop = new Properties();
			prop.load(input);
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}

	private void loadPlayer() throws SQLException {
		Set<String> splitOptions = new HashSet<String>();
		tableSet = selectTable(prop.getProperty("am_view_finalplayerstats"));
		while (tableSet.next()) {
			EntityPlayer player = new EntityPlayer(tableSet.getInt(1), tableSet.getString(2), tableSet.getString(3),
					tableSet.getInt(6));
			playersList.add(player);
			splitOptions.add(tableSet.getString(3));
			matchObjectList.add(player);
		}
		playerCategory = new ArrayList<String>(splitOptions);
	}

	private void loadMaps() throws SQLException {
		Set<String> splitOptions = new HashSet<String>();
		tableSet = selectTable(prop.getProperty("am_maps"));
		while (tableSet.next()) {
			EntityMap map = new EntityMap(tableSet.getInt(1), tableSet.getString(2), tableSet.getString(3));
			splitOptions.add(tableSet.getString(3));
			mapsList.add(map);
			matchObjectList.add(map);
		}
		mapCategory = new ArrayList<String>(splitOptions);

		for (int i = 0; i < mapsList.size(); i++) {
			selectTablePS(prop.getProperty("am_mapstats"), mapsList.get(i).getId());
			while (tableSet.next()) {
				mapsList.get(i).getMapStats().put(tableSet.getInt("player_id"), tableSet.getInt("sum"));
			}
		}
	}

	private void loadCivs() throws SQLException {
		Set<String> splitOptions = new HashSet<String>();
		tableSet = selectTable(prop.getProperty("am_nations"));
		while (tableSet.next()) {
			EntityCiv civ = new EntityCiv(tableSet.getInt(1), tableSet.getString(2), tableSet.getString(3));
			splitOptions.add(tableSet.getString(3));
			civsList.add(civ);
			matchObjectList.add(civ);
		}
		civCategory = new ArrayList<String>(splitOptions);

		for (int i = 0; i < civsList.size(); i++) {
			selectTablePS(prop.getProperty("am_nationstats"), civsList.get(i).getId());
			while (tableSet.next()) {
				civsList.get(i).getCivStats().put(tableSet.getInt("player_id"), tableSet.getInt("sum"));
			}
		}
	}

	private ResultSet selectTable(String query) throws SQLException {
		tableSet = statement.executeQuery(query);
		return tableSet;
	}

	private void selectTablePS(String query, int id) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, id);
		tableSet = ps.executeQuery();
	}

	private void insertTablePS(String query, String name, String category, int rating) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, name);
		ps.setString(2, category);
		ps.setInt(3, rating);
		ps.executeUpdate();
	}

	private void insertTablePS(String query, int map_id, String datetime) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, map_id);
		ps.setString(2, datetime);
		ps.executeUpdate();
	}

	private void insertTablePS(String query, Team team, int match_id) throws SQLException {
		for (int i = 0; i < team.getTeam().size(); i++) {
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setInt(1, team.getTeam_id());
			ps.setInt(2, team.getTeam().get(i).getId());
			ps.setInt(3, team.getTeam().get(i).getPickedCiv().getId());
			ps.setInt(4, team.getPlayerPoints());
			ps.setInt(5, team.getCivPoints());
			ps.setInt(6, team.getMapPoints());
			ps.setInt(7, (team.isTeamWinner()) ? 1 : 0); //F�r die Datenbank, sowohl Sqlite und MySql verzichten drauf
			ps.setInt(8, match_id);
			ps.executeUpdate();
		}
	}

	public void createNewEntity(String queryInsert, String name, String category, int rating,
			CheckBoxTreeItem<Object> tree) throws SQLException {
		setJDBC();
		insertTablePS(queryInsert, name, category, rating);
		includeNewObject(name, category, tree);
	}

	private void includeNewObject(String name, String category, CheckBoxTreeItem<Object> tree) {
		CheckBoxTreeItem<Object> box = new CheckBoxTreeItem<Object>(name);
		for (TreeItem<Object> child : tree.getChildren()) {
			if (child.getValue().equals(category)) {
				child.getChildren().add(box);
			}
		}
	}

	public void savePlayedGame(SessionData session, boolean isAuto) {
		
		try {
			setJDBC();
			conn.setAutoCommit(false);

			insertTablePS(prop.getProperty("am_matches"), session.getMap().getId(), session.getDateTime(isAuto));
			tableSet = selectTable(prop.getProperty("am_matches_last_id"));
			int match_id = tableSet.getInt(1);
			insertTablePS(prop.getProperty("am_match_stats"), session.getTeamA(),match_id);
			insertTablePS(prop.getProperty("am_match_stats"), session.getTeamB(),match_id);
			PrintTeamData print = new PrintTeamData();
			print.printTeam(session.getTeamA(), "Team A", session.getDateTime(isAuto));
			print.printTeam(session.getTeamB(), "Team B", session.getDateTime(isAuto));
			conn.commit();
			statement.close();
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public ArrayList<MatchObject> getMatchObjectList() {
		return matchObjectList;
	}

	public ArrayList<EntityMap> getMaps() {
		return mapsList;
	}

	public ArrayList<EntityCiv> getCivs() {
		return civsList;
	}

	public ArrayList<EntityPlayer> getPlayers() {
		return playersList;
	}
	
	public Properties getProp() {
		return prop;
	}

	public ArrayList<String> getPlayerCategory() {
		return playerCategory;
	}

	public ArrayList<String> getCivCategory() {
		return civCategory;
	}

	public ArrayList<String> getMapCategory() {
		return mapCategory;
	}
}
