package database;

import java.util.HashMap;

public class EntityCiv extends MatchObject {
	
	private HashMap<Integer, Integer> civStats;
	
	EntityCiv(int id, String name, String category){
		super(id, name, category);
		civStats = new HashMap<Integer, Integer>();
	}
	
	public HashMap<Integer, Integer> getCivStats() {
		return civStats;
	}
	
	public int getCivRating(int playerID){
		return civStats.get(playerID);
	}

	
}
