package database;

import java.util.HashMap;

public class EntityMap extends MatchObject{

	private HashMap<Integer, Integer> mapStats;
	
	EntityMap(int id, String name, String category){
		super(id, name, category);
		this.mapStats 	= new HashMap<Integer, Integer>();
	}
	
	public HashMap<Integer, Integer> getMapStats() {
		return mapStats;
	}
	
	public int getMapRating(int playerID){
		return mapStats.get(playerID);
	}
}
