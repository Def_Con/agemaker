package database;

public class EntityPlayer extends MatchObject {

	private int playerRating;
	private int entireRating;
	private EntityMap pickedMap;
	private EntityCiv pickedCiv;
	private int civRating;
	private int mapRating;

	EntityPlayer(int id, String name, String category, int playerRating) {
		super(id, name, category);
		this.playerRating = playerRating;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////// GETTER-METHODEN/////////////////////////////////////////////

	public int getPlayerRating() {
		return playerRating;
	}

	public EntityMap getPickedMap() {
		return pickedMap;
	}

	public EntityCiv getPickedCiv() {
		return pickedCiv;
	}

	public int getCivRating() {
		return civRating;
	}

	public int getMapRating() {
		return mapRating;
	}

	public int getEntireRating() {
		this.entireRating = this.mapRating + this.civRating + this.playerRating;
		return entireRating;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////// SETTER-METHODEN/////////////////////////////////////////////
	public void setPickedMap(EntityMap pickedMap) {
		this.pickedMap = pickedMap;
		this.setMapRating(this.getPickedMap().getMapRating(this.getId()));
	}

	public void setPickedCiv(EntityCiv pickedCiv) {
		this.pickedCiv = pickedCiv;
		this.setCivRating(this.getPickedCiv().getCivRating(this.getId()));
	}

	private void setCivRating(int civRating) {
		this.civRating = civRating;
	}

	private void setMapRating(int mapRating) {
		this.mapRating = mapRating;
	}
}
