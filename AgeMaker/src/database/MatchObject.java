package database;

public abstract class MatchObject implements Comparable<MatchObject>{

	private int id;
	private String name;
	private String category;

	MatchObject(int id, String name, String category) {
		this.id = id;
		this.name = name;
		this.category = category;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getCategory() {
		return category;
	}

	public MatchObject findMatchObject(String nameOfMatchObject) {
		if (nameOfMatchObject == this.getName()) {
			return this;
		}
		return null;
	}

	@Override
	public String toString() {
		return name;
	}

	
	public int compareTo(MatchObject object) {
		
		return this.getName().compareTo(object.getName());
	}
}
