package database;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

public class SessionData {

	private EntityMap map;
	private Team teamA, teamB;
	private Date dateOfToday;
	private DateFormat df;
	private String dateTime;
	
	public SessionData(){
		dateOfToday = new Date();
		df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	}
	
	public SessionData(LocalDate date, LocalTime time){
		dateTime = date.toString()+" "+time.toString();
	}
	
	public void setMap(EntityMap map) {
		this.map = map;
	}

	public void setTeamA(Team teamA) {
		this.teamA = teamA;
	}

	public void setTeamB(Team teamB) {
		this.teamB = teamB;
	}

	public EntityMap getMap() {
		return map;
	}

	public Team getTeamA() {
		return teamA;
	}

	public Team getTeamB() {
		return teamB;
	}
	
	public String getDateTimeManuallyGenerated(){
		return dateTime;
	}
	
	public String getDateTime(boolean isAutoGenerated){
		if(isAutoGenerated){
			return df.format(dateOfToday);
		}else{
			return this.getDateTimeManuallyGenerated();
		}
		
	}

}
