package database;

import java.util.List;

public class Team {
	
	private List<EntityPlayer> team;
	private int team_id;
	private boolean isTeamFav;
	private boolean isTeamCivFav;
	private boolean isTeamWinner;
	
	private int teamRating;
	private int teamCivRating;
	private int teamMapRating;
	private int teamEntireRating;
	
	private int playerPoints;
	private int mapPoints;
	private int civPoints;
	
	
	public Team(List<EntityPlayer> team){
		this.team = team;
	}
	
	public void addPlayerToTeam(EntityPlayer player){
		team.add(player);
	}
	
	public void setRatings(){
		for(int i=0;i<team.size();i++){
			teamRating 		 = teamRating + team.get(i).getPlayerRating();
			teamCivRating	 = teamCivRating + team.get(i).getCivRating();
			teamMapRating	 = teamMapRating + team.get(i).getMapRating();
		}
		teamEntireRating = teamRating + teamCivRating + teamMapRating;
	}
	
	public void calcTeamFav(int teamRating, int opponentTeamRating){
		isTeamFav = whoIsFav(teamRating, opponentTeamRating);
	}
	
	public void calcTeamCivFav(int teamRatingCiv, int opponentTeamRatingCiv) {
		isTeamCivFav= whoIsFav(teamRatingCiv, opponentTeamRatingCiv);
	}
	
	private boolean whoIsFav(int teamRatingCiv, int opponentTeamRatingCiv){
		if(teamRatingCiv > opponentTeamRatingCiv){
			return true;
		}
		else if(opponentTeamRatingCiv > teamRatingCiv){
			return false;
		}
		else{
			return true;
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////GETTER-METHODEN/////////////////////////////////////////////
	public List<EntityPlayer> getTeam() {
		return team;
	}
	
	public int getTeamRating() {
		return teamRating;
	}
	
	public int getTeamCivRating() {
		return teamCivRating;
	}
	
	public int getTeamMapRating() {
		return teamMapRating;
	}

	public int getTeamEntireRating() {
		return teamEntireRating;
	}
	
	public boolean isTeamFav() {
		return isTeamFav;
	}
	
	public boolean isTeamCivFav() {
		return isTeamCivFav;
	}
	
	public boolean isTeamWinner() {
		return isTeamWinner;
	}
	
	public int getPlayerPoints() {
		return playerPoints;
	}

	public int getMapPoints() {
		return mapPoints;
	}

	public int getCivPoints() {
		return civPoints;
	}
	
	public int getTeam_id() {
		return team_id;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////SETTER-METHODEN/////////////////////////////////////////////
	public void setTeamWinner(boolean isTeamWinner) {
		this.isTeamWinner = isTeamWinner;
		this.team_id = (isTeamWinner)? 2 : 1;
	}

	public void setPlayerPoints(int playerPoints) {
		this.playerPoints = playerPoints;
	}
	
	public void setMapPoints(int mapPoints) {
		this.mapPoints = mapPoints;
	}

	public void setCivPoints(int civPoints) {
		this.civPoints = civPoints;
	}
}
