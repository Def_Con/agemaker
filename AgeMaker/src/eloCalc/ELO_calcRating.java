package eloCalc;

public class ELO_calcRating {

	public static float Probability(float rating1, float rating2) {
		return 1.0f * 1.0f / (1 + 1.0f * (float) (Math.pow(10, 1.0f * (rating1 - rating2) / 400)));
	}

	// Function to calculate Elo rating
	// K is a constant.
	// d determines whether Player A wins
	// or Player B.
	public static float EloRating(float Ra, float Rb, int K, boolean d) {
		float oldRating = Ra;
		// To calculate the Winning
		// Probability of Player B
		float Pb = Probability(Ra, Rb);
		System.out.println(Pb);
		// To calculate the Winning
		// Probability of Player A
		float Pa = Probability(Rb, Ra);
		System.out.println(Pa);
		// Case -1 When Player A wins
		// Updating the Elo Ratings
		if (d == true) {
			Ra = Ra + K * (1 - Pa);
			Rb = Rb + K * (0 - Pb);
		}

		// Case -2 When Player B wins
		// Updating the Elo Ratings
		else {
			Ra = Ra + K * (0 - Pa);
			Rb = Rb + K * (1 - Pb);
		}

		System.out.print("Updated Ratings:-\n");

		System.out.print("Ra = " + (Math.round(Ra * 1000000.0) / 1000000.0) + " Rb = " + Math.round(Rb * 1000000.0) / 1000000.0);
		return Math.round(Math.abs(Ra-oldRating));
	}

}
