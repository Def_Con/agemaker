package eloCalc;

public class Elo_Calculation {
	
	private int ergebnisRating;
	private int eloPunkteHoch;
	private int eloPunkteTief;
	private double erwartungswert;
	private int teamRatingA, teamRatingB;
	private final int kFactor = 30;
	
	public Elo_Calculation(int teamRatingA, int teamRatingB){
		this.teamRatingA = teamRatingA;
		this.teamRatingB = teamRatingB;
	}
	
	public int calcEloRating(boolean fav_team, boolean isWinner){
		int parameter = setParameter(isWinner);
		double ergDouble;
		sortRating();
		
		if(fav_team==true){
			ergDouble = kFactor*(erwartungswert+parameter);
			ergDouble = (eloPunkteHoch)+ergDouble;
			ergebnisRating = (int) ergDouble;
			ergebnisRating = ergebnisRating-eloPunkteHoch;
			
			return (ergebnisRating == 0) ? 1 : ergebnisRating;
		}else{
			ergDouble = kFactor*(isWinner_NonFavTeam(isWinner)+parameter);
			ergDouble = (eloPunkteTief)+ergDouble;
			ergebnisRating = (int) ergDouble;
			ergebnisRating = ergebnisRating-eloPunkteTief;
			
			return (ergebnisRating == 0) ? -1 : ergebnisRating;
		}
	}
	
	public int getMapRating(boolean isWinner) {
		int mapRating = (isWinner) ? 10 : -10;
		return mapRating ;
	}
	
	private int setParameter(boolean isWinner){
		int parameter;
		if(isWinner==true){
			parameter	=1;
			return parameter;
		}else{
			parameter	=0;
			return parameter;
		}
	}
	
	private double isWinner_NonFavTeam(boolean isWinner){
		if(isWinner==true){
			erwartungswert=1+erwartungswert;
			erwartungswert = erwartungswert*(-1);
			return erwartungswert;
		}else{
			erwartungswert=1+erwartungswert;
			erwartungswert = erwartungswert*(-1);
			return erwartungswert;
		}
	}
	
	private void sortRating(){
		
		if(teamRatingA > teamRatingB){
			this.eloPunkteHoch = teamRatingA;
			this.eloPunkteTief = teamRatingB;
		}
		else if(teamRatingB > teamRatingA){
			this.eloPunkteHoch = teamRatingB;
			this.eloPunkteTief = teamRatingA;
		}
		else{
			this.eloPunkteHoch = teamRatingA;
			this.eloPunkteTief = teamRatingB;
		}
		eloExpectedValue();
	}
	
	private void eloExpectedValue(){
		
		double erg;
		
		erg = eloPunkteTief-eloPunkteHoch;
		erg = erg/400;
		erg = Math.pow(10.0,erg);
		erg = 1+erg;
		erwartungswert = 1/erg;
		erwartungswert = Math.round(10.0*erwartungswert)/10.0;
		erwartungswert = erwartungswert*(-1);
	}

}
