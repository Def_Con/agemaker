package language;

import java.util.Locale;
import java.util.ResourceBundle;

public class Language {
	
	private Locale locale;
	private ResourceBundle bundle;
	public static String currentLanguage;
	
	public ResourceBundle getBundle() {
		return bundle;
	}
	
	public Language(String lang) {
		currentLanguage = lang;
		
		locale = new Locale(lang);
		bundle= ResourceBundle.getBundle("language.lang",locale);
	}
}
