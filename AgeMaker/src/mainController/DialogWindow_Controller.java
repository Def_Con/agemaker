package mainController;

import java.sql.SQLException;
import java.util.ArrayList;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTimePicker;
import database.Database;
import database.EntityCiv;
import database.EntityMap;
import database.EntityPlayer;
import database.SessionData;
import database.Team;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.stage.Stage;
import language.Language;
import match.Match_Fix;

public class DialogWindow_Controller {

	@FXML
	private ToggleButton toggleWinnerSAG;
	@FXML
	private Button okButton, cancelButton, teamASaveBt, teamBSaveBt;
	@FXML
	private TextField textFieldName, textFieldRating;
	@FXML
	private ComboBox<String> comboBoxCategoryCP, comboBoxCategoryCC, comboBoxCategoryCM;
	@FXML
	private ComboBox<EntityMap> comboBoxMapSAG;
	@FXML
	private ListView<EntityPlayer> listViewPlayers, listViewTeamAPlayers, listViewTeamBPlayers;
	@FXML
	private ListView<EntityCiv> listViewCivs, listViewTeamACivs, listViewTeamBCivs;
	@FXML
	private JFXDatePicker datePicker;
	@FXML
	private JFXTimePicker timePicker;

	private Database db;

	private String team = "Kein Team";
	private int rating;
	private TreeView_Controller treeViewcontroller;

	public void initialize() {
		if (timePicker != null) {
			timePicker.setIs24HourView(true);
		}
		db = Database.getInstanz();
		db.reloadDB();
		fillComboBox(comboBoxCategoryCP, db.getPlayerCategory());
		fillComboBox(comboBoxCategoryCC, db.getCivCategory());
		fillComboBox(comboBoxCategoryCM, db.getMapCategory());
		fillListViewPlayer(listViewPlayers, db.getPlayers());
		fillListViewCiv(listViewCivs, db.getCivs());
		fillComboBoxMap(comboBoxMapSAG, db.getMaps());
	}

	private void fillComboBox(ComboBox<String> comboBoxCategory, ArrayList<String> list) {
		if (comboBoxCategory != null) {
			comboBoxCategory.getItems().addAll(list);
			comboBoxCategory.getSelectionModel().selectFirst();
		}
	}

	private void fillComboBoxMap(ComboBox<EntityMap> comboBox, ArrayList<EntityMap> list) {
		if (comboBox != null) {
			for (int i = 0; i < list.size(); i++) {
				comboBox.getItems().add(list.get(i));
			}
		}
	}

	private void fillListViewCiv(ListView<EntityCiv> listView, ArrayList<EntityCiv> list) {
		if (listView != null) {
			for (int i = 0; i < list.size(); i++) {
				listView.getItems().add(list.get(i));
			}
		}
	}

	private void fillListViewPlayer(ListView<EntityPlayer> listView, ArrayList<EntityPlayer> list) {
		if (listView != null) {
			for (int i = 0; i < list.size(); i++) {
				listView.getItems().add((list.get(i)));
			}
		}
	}

	@FXML
	public void pressedCancelButton() {
		closeStage(cancelButton);
	}

	// Erstelle einen neuen Spieler
	@FXML
	public void pressedOKButtonCP() throws SQLException {
		String inputTFName = textFieldName.getText().trim();
		String inputTFRating = textFieldRating.getText().trim();
		String inputCategpory = comboBoxCategoryCP.getValue();
		if (checkTFNameInput(inputTFName) && checkTFRatingInput(inputTFRating)) {
			db.createNewEntity(db.getProp().getProperty("am_players_insert"), inputTFName, inputCategpory, rating,
					treeViewcontroller.getPlayer_MainRoot());
		}
		closeStage(okButton);
	}

	// Erstelle ein neues Volk
	@FXML
	public void pressedOKButtonCC() {
		closeStage(okButton);
	}

	// Erstelle eine neue Karte
	@FXML
	public void pressedOKButtonCM() throws SQLException {
		String inputTFName = textFieldName.getText().trim();
		String inputTFRating = textFieldRating.getText().trim();
		String inputCategpory = comboBoxCategoryCM.getValue();
		if (checkTFNameInput(inputTFName) && checkTFRatingInput(inputTFRating)) {
			db.createNewEntity(db.getProp().getProperty("am_maps_insert"), inputTFName, inputCategpory, rating,
					treeViewcontroller.getMap_MainRoot());
		}
		closeStage(okButton);
	}

	@FXML
	public void saveTeamA() {
		team = "TeamA";
		closeStage(teamASaveBt);
	}

	@FXML
	public void saveTeamB() {
		team = "TeamB";
		closeStage(teamBSaveBt);
	}

	/*
	 * Methode(n) f�r das DialogFenster SaveAdditionMatch. Spiele die nicht zuf�llig
	 * generiert wurden k�nnen in diesem Fenster eingetragen werden. Sie werden in
	 * der Datenbank eingetragen.
	 */
	@FXML
	public void pressedSaveButtonSAG() {

		if (checkConditions() == 7) {

			SessionData session = new SessionData(datePicker.getValue(), timePicker.getValue());
			Match_Fix fixMatchTeamA = new Match_Fix(listViewTeamAPlayers.getItems(), listViewTeamACivs.getItems(),
					comboBoxMapSAG.getSelectionModel().getSelectedItem());
			Match_Fix fixMatchTeamB = new Match_Fix(listViewTeamBPlayers.getItems(), listViewTeamBCivs.getItems(),
					comboBoxMapSAG.getSelectionModel().getSelectedItem());

			Team teamA = fixMatchTeamA.entityToPlayerAssignment();
			Team teamB = fixMatchTeamB.entityToPlayerAssignment();
			session.setMap(comboBoxMapSAG.getSelectionModel().getSelectedItem());
			session.setTeamA(teamA);
			session.setTeamB(teamB);
			teamA.setRatings();
			teamB.setRatings();
			teamA.calcTeamFav(teamA.getTeamEntireRating(), teamB.getTeamEntireRating());
			teamB.calcTeamFav(teamB.getTeamEntireRating(), teamA.getTeamEntireRating());
			teamA.calcTeamCivFav(teamA.getTeamCivRating(), teamB.getTeamCivRating());
			teamB.calcTeamCivFav(teamB.getTeamCivRating(), teamA.getTeamCivRating());

			if (toggleWinnerSAG.isSelected()) {
				System.out.println("Team B ist gewinner");
				fixMatchTeamA.saveData(teamB, teamA);
				db.savePlayedGame(session, false);
			} else {
				System.out.println("Team A ist gewinner");
				fixMatchTeamA.saveData(teamA, teamB);
				db.savePlayedGame(session, false);
			}

			closeStage(okButton);
		}
	}

	@FXML
	public void changeWinnerTeam() {
		Language lang = new Language(Language.currentLanguage);
		if (toggleWinnerSAG.isSelected()) {
			toggleWinnerSAG.setText(lang.getBundle().getString("dwSAG_TeamBToggle"));
		} else {
			toggleWinnerSAG.setText(lang.getBundle().getString("dwSAG_TeamAToggle"));
		}
	}

	private int checkConditions() {
		int counter = 0;
		if (listViewTeamAPlayers.getItems().size() > 0)
			counter++;
		if (listViewTeamBPlayers.getItems().size() > 0)
			counter++;
		if (listViewTeamAPlayers.getItems().size() == listViewTeamACivs.getItems().size())
			counter++;
		if (listViewTeamBPlayers.getItems().size() == listViewTeamBCivs.getItems().size())
			counter++;
		if (comboBoxMapSAG.getSelectionModel().isEmpty() == false)
			counter++;
		if (datePicker.getValue() != null)
			counter++;
		if (timePicker.getValue() != null)
			counter++;
		return counter;
	}

	private void closeStage(Button button) {
		Stage stage = (Stage) button.getScene().getWindow();
		stage.close();
	}

	private boolean checkTFNameInput(String inputTFName) {
		if (inputTFName.length() > 0) {
			return true;
		}
		return false;
	}

	private boolean checkTFRatingInput(String inputTFRating) {
		if (inputTFRating.length() > 0) {
			return checkInputIsInteger(inputTFRating);
		}
		return false;
	}

	private boolean checkInputIsInteger(String input) {
		try {
			rating = Integer.parseInt(input);
			return true;
		} catch (Exception ex) {
			System.out.println("Kein Integer");
		}
		return false;
	}

	public String getTeam() {
		return team;
	}

	/*----------------Methoden f�r die DialogBox SaveAdditionMatch----------------*/

	@FXML
	public void buttonLRTeamAPlayers() {
		transferToListView(listViewPlayers, listViewTeamAPlayers, false);
	}

	@FXML
	public void buttonRLTeamAPlayers() {
		transferToListView(listViewTeamAPlayers, listViewPlayers, true);
	}

	@FXML
	public void buttonLRTeamBPlayers() {
		transferToListView(listViewPlayers, listViewTeamBPlayers, false);
	}

	@FXML
	public void buttonRLTeamBPlayers() {
		transferToListView(listViewTeamBPlayers, listViewPlayers, true);
	}

	@FXML
	public void buttonRLTeamACivs() {
		copyToListView(listViewCivs, listViewTeamACivs, false);
	}

	@FXML
	public void buttonLRTeamACivs() {
		copyToListView(listViewTeamACivs, listViewCivs, true);
	}

	@FXML
	public void buttonRLTeamBCivs() {
		copyToListView(listViewCivs, listViewTeamBCivs, false);
	}

	@FXML
	public void buttonLRTeamBCivs() {
		copyToListView(listViewTeamBCivs, listViewCivs, true);
	}

	private void transferToListView(ListView<EntityPlayer> from, ListView<EntityPlayer> to,
			boolean isFromRightListView) {
		if (from.getSelectionModel().isEmpty() == false) {
			if (isFromRightListView) {
				to.getItems().add(from.getSelectionModel().getSelectedItem());
				from.getItems().remove(from.getSelectionModel().getSelectedItem());
			} else if (to.getItems().size() < 4) {
				to.getItems().add(from.getSelectionModel().getSelectedItem());
				from.getItems().remove(from.getSelectionModel().getSelectedItem());
			}
		}
	}

	private void copyToListView(ListView<EntityCiv> from, ListView<EntityCiv> to, boolean isFromLeftListView) {
		if (from.getSelectionModel().isEmpty() == false) {
			if (isFromLeftListView) {
				from.getItems().remove(from.getSelectionModel().getSelectedItem());
			} else if (to.getItems().size() < 4) {
				to.getItems().add(from.getSelectionModel().getSelectedItem());
			}
		}
	}

	/*----------------Initialisierungs Methoden----------------*/

	public void init(TreeView_Controller treeViewcontroller) {
		this.treeViewcontroller = treeViewcontroller;
	}
}
