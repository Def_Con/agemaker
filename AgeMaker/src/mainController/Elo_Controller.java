package mainController;

import java.util.ArrayList;
import java.util.List;

import database.EntityPlayer;
import database.Team;
import eloCalc.ELO_calcRating;
import eloCalc.Elo_Calculation;
import eloCalc.Elo_Table_Data;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import eloCalc.Elo_Calculation;

public class Elo_Controller {

	@FXML TableView<Elo_Table_Data> ratingTableA;
	@FXML TableView<Elo_Table_Data> ratingTableB;

	@FXML TableView<Elo_Table_Data> eloTableA;
	@FXML TableView<Elo_Table_Data> eloTableB;

	@FXML TableView<Elo_Table_Data> predictionTableA;
	@FXML TableView<Elo_Table_Data> predictionTableB;

	@FXML ArrayList<TableColumn<Elo_Table_Data, String>> tableColumnList;
	@FXML ArrayList<TableView<Elo_Table_Data>> tableViewList;
	
	private Elo_Calculation calc, calcNation;
	
	public void createTables(Team teamA, Team teamB){
		
		
		calc = new Elo_Calculation(teamA.getTeamEntireRating(), teamB.getTeamEntireRating());
		calcNation = new Elo_Calculation(teamA.getTeamCivRating(), teamB.getTeamCivRating());
		
		ObservableList<Elo_Table_Data> dataRatingTableA			= FXCollections.observableArrayList();
		ObservableList<Elo_Table_Data> dataRatingTableB 		= FXCollections.observableArrayList();
		ObservableList<Elo_Table_Data> dataEloTableA			= FXCollections.observableArrayList();
		ObservableList<Elo_Table_Data> dataEloTableB			= FXCollections.observableArrayList();
		ObservableList<Elo_Table_Data> dataPredictionTableA		= FXCollections.observableArrayList();
		ObservableList<Elo_Table_Data> dataPredictionTableB		= FXCollections.observableArrayList();

		setTables(teamA.getTeam(), dataRatingTableA);
		setTables(teamB.getTeam(), dataRatingTableB);

		dataEloTableA.add(new Elo_Table_Data(	teamA.getTeamEntireRating(), teamA.getTeamRating(),
												teamA.getTeamCivRating(), teamA.getTeamMapRating()));

		dataEloTableB.add(new Elo_Table_Data(	teamB.getTeamEntireRating(), teamB.getTeamRating(),
												teamB.getTeamCivRating(), teamB.getTeamMapRating()));

		dataPredictionTableA.add(new Elo_Table_Data(calc.calcEloRating(teamA.isTeamFav(),true),
													calc.calcEloRating(teamA.isTeamFav(),false),
													calcNation.calcEloRating(teamA.isTeamCivFav(),true),
													calcNation.calcEloRating(teamA.isTeamCivFav(),false),
													calc.getMapRating(true),
													calc.getMapRating(false)));
		
//		dataPredictionTableA.add(new Elo_Table_Data((int)ELO_calcRating.EloRating(teamA.getTeamEntireRating(), teamB.getTeamEntireRating(), 30, true),
//													(int)ELO_calcRating.EloRating(teamA.getTeamEntireRating(), teamB.getTeamEntireRating(), 30, false),
//													(int)ELO_calcRating.EloRating(teamA.getTeamCivRating(), teamB.getTeamCivRating(), 30, true),
//													(int)ELO_calcRating.EloRating(teamA.getTeamCivRating(), teamB.getTeamCivRating(), 30, false),
//													calc.getMapRating(true),
//													calc.getMapRating(false)));
		
		ELO_calcRating.EloRating((float)teamA.getTeamEntireRating(), (float)teamB.getTeamEntireRating(), 30, true);
		ELO_calcRating.EloRating((float)teamA.getTeamEntireRating(), (float)teamB.getTeamEntireRating(), 30, false);
		

//		dataPredictionTableB.add(new Elo_Table_Data((int)ELO_calcRating.EloRating(teamB.getTeamEntireRating(), teamA.getTeamEntireRating(), 30, true),
//													(int)ELO_calcRating.EloRating(teamB.getTeamEntireRating(), teamA.getTeamEntireRating(), 30, false),
//													(int)ELO_calcRating.EloRating(teamB.getTeamCivRating(), teamA.getTeamCivRating(), 30, true),
//													(int)ELO_calcRating.EloRating(teamB.getTeamCivRating(), teamA.getTeamCivRating(), 30, false),
//													calc.getMapRating(true),
//													calc.getMapRating(false)));

		dataPredictionTableB.add(new Elo_Table_Data(calc.calcEloRating(teamB.isTeamFav(),true),
													calc.calcEloRating(teamB.isTeamFav(),false),
													calcNation.calcEloRating(teamB.isTeamCivFav(),true),
													calcNation.calcEloRating(teamB.isTeamCivFav(),false),
													calc.getMapRating(true),
													calc.getMapRating(false)));

		ratingTableA.setItems(dataRatingTableA);
		ratingTableB.setItems(dataRatingTableB);

		eloTableA.setItems(dataEloTableA);
		eloTableB.setItems(dataEloTableB);

		predictionTableA.setItems(dataPredictionTableA);
		predictionTableB.setItems(dataPredictionTableB);
		
		/*	Die Schleife durchl�uft alle TableColumn Elemente der ArrayListe welche zuvor im FXML f�r den Elo_Controller zusammengefasst wurde. 
		 * 	Die Elemente sind ebefalls in der selben FXML festgehalten. 
		 * 	Die Elemente besitzen die selben Namen wie die in der Elo_Table_Data Klasse, bis auf die Endung A oder B die in der Schleife abgeschnitten wird.
		 */
		for(TableColumn<Elo_Table_Data, String> column : tableColumnList){
			String cutoff = column.getId().substring(0,column.getId().length()-1);
			column.setCellValueFactory(new PropertyValueFactory<Elo_Table_Data, String>(cutoff));
		}

	}

	private void setTables(List<EntityPlayer> list, ObservableList<Elo_Table_Data> ratingTable){
		for(int i=0;i<list.size();i++){
			ratingTable.add(new Elo_Table_Data(	list.get(i).getName(), list.get(i).getPlayerRating(), 
												list.get(i).getCivRating(), list.get(i).getMapRating()));
		}
	}

	public void clearTables(){
		ratingTableA.getItems().clear();
		ratingTableB.getItems().clear();

		eloTableA.getItems().clear();
		eloTableB.getItems().clear();

		predictionTableA.getItems().clear();
		predictionTableB.getItems().clear();
	}
}
