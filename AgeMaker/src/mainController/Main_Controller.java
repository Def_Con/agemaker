package mainController;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TreeView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import language.Language;

public class Main_Controller {

	@FXML private TreeView_Controller treeViewcontroller;

	@FXML private TreeView<Object> treeview_Players;
	@FXML private TreeView<Object> treeview_Maps;
	@FXML private TreeView<Object> treeview_Civs;

	@FXML private Accordion accordion;
	@FXML private TitledPane playersPane;

	@FXML private Match_Controller tabMatch_Controller;
	@FXML private Elo_Controller tabeElo_Controller;
	@FXML private Menu_Controller menu_Controller;
	
	public void initialize(){
		
		treeViewcontroller = new TreeView_Controller(treeview_Players, treeview_Maps, treeview_Civs);
		tabMatch_Controller.init(treeViewcontroller);
		tabMatch_Controller.init(tabeElo_Controller);
		tabMatch_Controller.init(this);
		menu_Controller.init(this);
		accordion.setExpandedPane(playersPane);
	}
	
	public DialogWindow_Controller dialogBox(String pathOfFXML, String title){
		FXMLLoader fxmlLoader = new FXMLLoader();
		try {
			fxmlLoader.setResources(new Language(Language.currentLanguage).getBundle());
			Parent root = fxmlLoader.load(getClass().getResource(pathOfFXML).openStream());
			DialogWindow_Controller dwController = (DialogWindow_Controller)fxmlLoader.getController();
			dwController.init(treeViewcontroller);
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.setTitle(title);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setResizable(false);
			stage.showAndWait();
			return dwController;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
