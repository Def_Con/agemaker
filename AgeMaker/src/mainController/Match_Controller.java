package mainController;

import java.awt.Desktop;
import java.io.File;
import java.util.ArrayList;
import org.controlsfx.control.PopOver;
import database.Database;
import database.EntityMap;
import database.EntityCiv;
import database.EntityPlayer;
import database.MatchObject;
import database.SessionData;
import database.Team;
import eloCalc.Elo_Calculation;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.VBox;
import language.Language;
import match.Match_Random;

public class Match_Controller {

	@FXML
	private TreeView_Controller treeViewController;
	@FXML
	private Elo_Controller tabElo_Controller;
	@FXML
	private Button save, shuffle, reset;
	@FXML
	private ToggleButton mirrorCivs, doubleCivs, fixPlayers, fixMap;
	@FXML
	private ArrayList<Button> btList_TeamA, btList_TeamB, btList_CivA, btList_CivB, btList_Map;

	private PopOver popOver;
	private Label warningLabel;
	private Database db;
	private boolean isCheck_Conditions;
	private final String pictureFormat[] = { ".jpg", ".Bmp", ".png" };

	// ArrayListen welche die ausgew�hlten(Checkbox Haken)
	// Spieler/Nationen/Karten enthalten
	private ArrayList<EntityMap> shuffleMapsList;
	private ArrayList<EntityCiv> shuffleCivsList;
	private ArrayList<EntityPlayer> shufflePlayersList;

	private SessionData session;
	private Team teamA, teamB;
	private Elo_Calculation calc, calcCiv;
	private Language lang;
	private Main_Controller main_Controller;
	private DialogWindow_Controller dw_Controller;

	public void initialize() {
		lang = new Language(Language.currentLanguage);
		db = Database.getInstanz();

		popOver = new PopOver();
		warningLabel = new Label();
		VBox vBox = new VBox(warningLabel);
		vBox.setPadding(new Insets(20));
		popOver.setContentNode(vBox);

		teamA = new Team(new ArrayList<EntityPlayer>());
		teamB = new Team(new ArrayList<EntityPlayer>());
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/*
	 * Methoden f�r Random Matchmaking und Teilauslosung
	 */
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	@FXML
	public void shuffle() {
		session = new SessionData();
		shuffleMapsList = new ArrayList<EntityMap>();
		shuffleCivsList = new ArrayList<EntityCiv>();
		shufflePlayersList = new ArrayList<EntityPlayer>();

		addSelectedPlayers(treeViewController.passSelectedTreeItems(treeViewController.getPlayer_MainRoot()));
		addSelectedCivs(treeViewController.passSelectedTreeItems(treeViewController.getCiv_MainRoot()));
		addSelectedMaps(treeViewController.passSelectedTreeItems(treeViewController.getMap_MainRoot()));

		isCheck_Conditions = proofMatchConditions();

		if (isCheck_Conditions == true) {
			fixPlayers.setDisable(false);
			fixMap.setDisable(false);
			save.setDisable(false);
			auslosungMatch();
			showMatchScreen();
			tabElo_Controller.createTables(teamA, teamB);
		}
	}

	private void addSelectedPlayers(ArrayList<String> list) {
		for (int i = 0; i < db.getPlayers().size(); i++) {
			for (int j = 0; j < list.size(); j++) {
				MatchObject object = db.getPlayers().get(i).findMatchObject(list.get(j));
				if (object != null) {
					shufflePlayersList.add((EntityPlayer) object);
				}
			}
		}
	}

	private void addSelectedCivs(ArrayList<String> list) {
		for (int i = 0; i < db.getCivs().size(); i++) {
			for (int j = 0; j < list.size(); j++) {
				MatchObject object = db.getCivs().get(i).findMatchObject(list.get(j));
				if (object != null) {
					shuffleCivsList.add((EntityCiv) object);
				}
			}
		}
	}

	private void addSelectedMaps(ArrayList<String> list) {
		for (int i = 0; i < db.getMaps().size(); i++) {
			for (int j = 0; j < list.size(); j++) {
				MatchObject object = db.getMaps().get(i).findMatchObject(list.get(j));
				if (object != null) {
					shuffleMapsList.add((EntityMap) object);
				}
			}
		}
	}

	private boolean proofMatchConditions() {
		if (shufflePlayersList.size() > 8) {
			warningLabel.setText(lang.getBundle().getString("ppOver_PlayersMax"));
			popOver.show(shuffle);
			return false;
		} else if (shufflePlayersList.size() < 2) {
			warningLabel.setText(lang.getBundle().getString("ppOver_PlayersMin"));
			popOver.show(shuffle);
			return false;
		} else if (shuffleMapsList.size() == 0 || shuffleCivsList.size() == 0) {
			warningLabel.setText(lang.getBundle().getString("ppOver_MatchCiv"));
			popOver.show(shuffle);
			return false;
		} else {
			return true;
		}
	}

	private void auslosungMatch() {
		Match_Random matchUpRandom = new Match_Random();

		if (fixPlayers.isSelected()) {
			teamA = new Team(teamA.getTeam());
			teamB = new Team(teamB.getTeam());
			ArrayList<EntityPlayer> merged = new ArrayList<EntityPlayer>();
			merged.addAll(teamA.getTeam());
			merged.addAll(teamB.getTeam());
			if (fixMap.isSelected() == false) {
				session.setMap(matchUpRandom.setRandomMap(merged, shuffleMapsList));
			}
			matchUpRandom.setNationsForPlayers(merged, shuffleCivsList, doubleCivs.isSelected(),
					mirrorCivs.isSelected());
			matchUpRandom.writeMatchHalfwayRandom(teamA, teamB);
		} else {
			teamA = new Team(new ArrayList<EntityPlayer>());
			teamB = new Team(new ArrayList<EntityPlayer>());
			if (fixMap.isSelected() == false) {
				session.setMap(matchUpRandom.setRandomMap(shufflePlayersList, shuffleMapsList));
			}
			matchUpRandom.setNationsForPlayers(shufflePlayersList, shuffleCivsList, doubleCivs.isSelected(),
					mirrorCivs.isSelected());
			matchUpRandom.writeMatchFullRandom(teamA, teamB);
		}

		session.setTeamA(teamA);
		session.setTeamB(teamB);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/*
	 * Methoden die die Daten auf den Buttons abbilden.
	 */
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////

	private void showMatchScreen() {
		btList_Map.get(0).setText(session.getMap().getName());
		setTeams_PlayerButtons(btList_TeamA, btList_CivA, teamA);
		setTeams_PlayerButtons(btList_TeamB, btList_CivB, teamB);
	}

	private void setTeams_PlayerButtons(ArrayList<Button> btListPlayer, ArrayList<Button> btListNation, Team team) {

		setButtonsText(team.getTeam().size(), btListPlayer, btListNation, team);
		setButtonsVisibility(team.getTeam().size(), btListPlayer);
		setButtonsVisibility(team.getTeam().size(), btListNation);
	}

	private void setButtonsText(int teamSize, ArrayList<Button> btListPlayer, ArrayList<Button> btListNation,
			Team team) {
		for (int i = 0; i < teamSize; i++) {
			btListPlayer.get(i).setText(team.getTeam().get(i).getName());
			btListNation.get(i).setText(team.getTeam().get(i).getPickedCiv().getName());
		}
	}

	private void setButtonsVisibility(int teamSize, ArrayList<Button> btList) {
		for (int i = 0; i < teamSize; i++) {
			btList.get(i).setVisible(true);
		}
		for (int i = teamSize; i < btList.size(); i++) {
			btList.get(i).setVisible(false);
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/*
	 * Controller Klassen werden hier initialisert
	 */
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////

	public void init(TreeView_Controller controller) {
		this.treeViewController = controller;
	}

	public void init(Elo_Controller controller) {
		this.tabElo_Controller = controller;
	}

	public void init(Main_Controller main_Controller) {
		this.main_Controller = main_Controller;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/*
	 * SAVE-Methoden
	 */
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////

	@FXML
	public void save() {
		dw_Controller = (DialogWindow_Controller) main_Controller.dialogBox(
				"/matchMakerView/MatchMaker_DialogBox_SaveMatch.fxml",
				lang.getBundle().getString("dwSave_HeaderLabel"));

		if (dw_Controller.getTeam().equals("TeamA")) {
			saveData(teamA, teamB);
		} else if (dw_Controller.getTeam().equals("TeamB")) {
			saveData(teamB, teamA);
		}
	}

	private void saveData(Team winnerTeam, Team loserTeam) {
		calc = new Elo_Calculation(winnerTeam.getTeamEntireRating(), loserTeam.getTeamEntireRating());
		calcCiv = new Elo_Calculation(winnerTeam.getTeamCivRating(), loserTeam.getTeamCivRating());
		setTeamData(winnerTeam, true);
		setTeamData(loserTeam, false);
		db.savePlayedGame(session, true);
		save.setDisable(true);
	}

	private void setTeamData(Team team, boolean isWinner) {
		team.setTeamWinner(isWinner);
		team.setPlayerPoints(calc.calcEloRating(team.isTeamFav(), team.isTeamWinner()));
		team.setCivPoints(calcCiv.calcEloRating(team.isTeamCivFav(), team.isTeamWinner()));
		team.setMapPoints(calc.getMapRating(team.isTeamWinner()));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/*
	 * Map-Preview
	 */
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////

	@FXML
	public void mapPreviewButton() {
		String mapName = btList_Map.get(0).getText();
		Desktop desktop = Desktop.getDesktop();
		File jpg;
		int counter =0;
		for (int i = 0; i < pictureFormat.length; i++) {
			try {
				jpg = new File("resources/map_pictures/" + mapName + pictureFormat[i]);
				counter++;
				if (jpg.isFile() == false) {
					if(counter>=pictureFormat.length) {
						desktop.open(new File("resources/map_pictures/" + "keine_vorschau.png"));
					}
				} else {
					desktop.open(jpg);
					break;
				}

			} catch (Exception ex) {}
		}
	}
}
