package mainController;

import java.io.IOException;
import java.util.prefs.Preferences;
import javafx.fxml.FXML;
import language.Language;
import matchMakerMain.MatchMaker_Main;

public class Menu_Controller {
	
	private Language lang;
	private Main_Controller main_Controller;

	
	public void initialize(){
		lang = new Language(Language.currentLanguage);
	}
	
	@FXML
	public void newMatch(){
		main_Controller.dialogBox("/matchMakerView/MatchMaker_DialogBox_SaveAdditionMatch.fxml", lang.getBundle().getString("dwSAG_HeaderLabel"));
	}
	
	@FXML
	public void newPlayer(){
		main_Controller.dialogBox("/matchMakerView/MatchMaker_DialogBox_CreatePlayer.fxml", lang.getBundle().getString("dwCP_HeaderLabel"));
	}
	
	@FXML
	public void newMap(){
		main_Controller.dialogBox("/matchMakerView/MatchMaker_DialogBox_CreateMap.fxml", lang.getBundle().getString("dwCM_HeaderLabel"));
	}
	
	@FXML
	public void newCiv(){
		main_Controller.dialogBox("/matchMakerView/MatchMaker_DialogBox_CreateCiv.fxml", lang.getBundle().getString("dwCC_HeaderLabel"));
	}
	
	@FXML
	public void changeLang_GER() throws IOException{
		Preferences pref = Preferences.userNodeForPackage(MatchMaker_Main.class);
		pref.put("Language", "de");
	}
	
	@FXML
	public void changeLang_ENG() throws IOException{
		Preferences pref = Preferences.userNodeForPackage(MatchMaker_Main.class);
		pref.put("Language", "en");
	}

	public void init(Main_Controller main_Controller) {
		this.main_Controller = main_Controller;
	}
}
