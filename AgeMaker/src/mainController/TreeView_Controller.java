package mainController;

import java.util.ArrayList;
import database.Database;
import database.MatchObject;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.CheckBoxTreeCell;
import language.Language;

public class TreeView_Controller {

	private CheckBoxTreeItem<Object> player_MainRoot, map_MainRoot, civ_MainRoot;
	private Database db;

	public TreeView_Controller(TreeView<Object> treeview_Players, TreeView<Object> treeview_Maps, TreeView<Object> treeview_Civs) {
		
		Language lang = new Language(Language.currentLanguage);
		db = Database.getInstanz();
		
		player_MainRoot = new CheckBoxTreeItem<Object>(lang.getBundle().getString("tv_PlayerRoot"));
		civ_MainRoot = new CheckBoxTreeItem<Object>(lang.getBundle().getString("tv_CivRoot"));
		map_MainRoot = new CheckBoxTreeItem<Object>(lang.getBundle().getString("tv_MapRoot"));
		
		createTreeViewChilds(player_MainRoot, db.getPlayerCategory());
		createTreeViewChilds(civ_MainRoot, db.getCivCategory());
		createTreeViewChilds(map_MainRoot, db.getMapCategory());
		
		// Damit die Roots ausgeklappt angezeigt werden
		player_MainRoot.setExpanded(true);
		map_MainRoot.setExpanded(true);
		civ_MainRoot.setExpanded(true);
		
		fillTreeView(player_MainRoot,db.getMatchObjectList(), true);
		fillTreeView(map_MainRoot,db.getMatchObjectList(), false);
		fillTreeView(civ_MainRoot,db.getMatchObjectList(), false);
		
		// TreeCell sind f�r die Checkboxes im Treeview
		treeview_Players.setCellFactory(CheckBoxTreeCell.forTreeView());
		treeview_Maps.setCellFactory(CheckBoxTreeCell.forTreeView());
		treeview_Civs.setCellFactory(CheckBoxTreeCell.forTreeView());
		
		treeview_Players.setRoot(player_MainRoot);
		treeview_Maps.setRoot(map_MainRoot);
		treeview_Civs.setRoot(civ_MainRoot);
	}

	private void createTreeViewChilds(CheckBoxTreeItem<Object> root, ArrayList<String> categoryOfChilds) {
		int size = categoryOfChilds.size();
		for (int i = 0; i < size; i++) {
				CheckBoxTreeItem<Object> treeItem = new CheckBoxTreeItem<Object>(categoryOfChilds.get(i));
				root.getChildren().add(treeItem);
		}
	}

	private void fillTreeView(CheckBoxTreeItem<Object> root, ArrayList<MatchObject> matchObjectList, boolean isExpanded) {
		int size = root.getChildren().size();
		for (int i = 0; i < matchObjectList.size(); i++) {
			for (int j = 0; j < size; j++) {
				if (root.getChildren().get(j).getValue().equals(matchObjectList.get(i).getCategory())) {
					CheckBoxTreeItem<Object> box = new CheckBoxTreeItem<Object>(matchObjectList.get(i));
					root.getChildren().get(j).getChildren().add(box);
					root.getChildren().get(j).setExpanded(isExpanded);
				}
			}
		}
	}
	
	public ArrayList<String> passSelectedTreeItems(TreeItem<Object> root){
		ArrayList<String> selectedItems = new ArrayList<String>();
		return seekForItems(root, selectedItems);
    }
	
	private ArrayList<String> seekForItems(TreeItem<Object> root, ArrayList<String> selectedItems){
		for(TreeItem<Object> child: root.getChildren()){
            if(child.isLeaf()&&((CheckBoxTreeItem<Object>)child).isSelected()==true){
                selectedItems.add(child.getValue().toString());          
            } else {
            	seekForItems(child, selectedItems);
            }
        }
		return selectedItems;
	}

	public CheckBoxTreeItem<Object> getPlayer_MainRoot() {
		return player_MainRoot;
	}

	public CheckBoxTreeItem<Object> getMap_MainRoot() {
		return map_MainRoot;
	}

	public CheckBoxTreeItem<Object> getCiv_MainRoot() {
		return civ_MainRoot;
	}
}
