package match;

import java.util.List;
import database.EntityCiv;
import database.EntityMap;
import database.EntityPlayer;
import database.Team;
import eloCalc.Elo_Calculation;
import javafx.collections.ObservableList;

public class Match_Fix {
	
	private List<EntityPlayer> players;
	private List<EntityCiv> civs;
	private EntityMap map;
	private Elo_Calculation calc;
	private Elo_Calculation calcCiv;
	
	public Match_Fix(ObservableList<EntityPlayer> players, ObservableList<EntityCiv> civs, EntityMap map){
		this.players 	= players;
		this.civs		= civs;
		this.map		= map;
	}
	
	public Team entityToPlayerAssignment(){
		for(int i=0;i<players.size();i++){
			players.get(i).setPickedCiv(civs.get(i));
			players.get(i).setPickedMap(map);
		}
		return new Team(players);
	}
	
	public void saveData(Team winnerTeam, Team loserTeam) {
		calc = new Elo_Calculation(winnerTeam.getTeamEntireRating(), loserTeam.getTeamEntireRating());
		calcCiv = new Elo_Calculation(winnerTeam.getTeamCivRating(), loserTeam.getTeamCivRating());
		setTeamData(winnerTeam, true);
		setTeamData(loserTeam, false);
	}
	
	private void setTeamData(Team team, boolean isWinner){
		team.setTeamWinner(isWinner);
		team.setPlayerPoints(calc.calcEloRating(team.isTeamFav(), team.isTeamWinner()));
		team.setCivPoints(calcCiv.calcEloRating(team.isTeamCivFav(), team.isTeamWinner()));
		team.setMapPoints(calc.getMapRating(team.isTeamWinner()));
	}

}
