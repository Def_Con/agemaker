package match;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;
import database.EntityPlayer;
import database.EntityMap;
import database.EntityCiv;
import database.Team;

public class Match_Random {
	
	private ArrayList<EntityPlayer> players;
	private int random;
	private Random randomNumber;
	
	public Match_Random(){
		randomNumber = new Random();
	}

	/*---------------- Auslosung der Karten welche sich in der matchMapsList befindet ----------------*/

	public EntityMap setRandomMap(ArrayList<EntityPlayer> players, ArrayList<EntityMap> maps){
		random = randomNumber.nextInt(maps.size());
		EntityMap pickedMap = maps.get(random);
		
	    for(int i=0;i<players.size();i++){
//	    	players.get(i).setMapRating(maps.get(random).getMapRating(players.get(i).getId()));
	    	players.get(i).setPickedMap(pickedMap);
	    }
	    return pickedMap;
	}
	
	public void setNationsForPlayers(ArrayList<EntityPlayer> players, ArrayList<EntityCiv> civs, boolean isDoubleNation, boolean isMirror){
		this.players = players;
		// Wichtig zu beachten, hier wird die Sortierung schon vor der Vergabe des NationsRating erledigt. Sonst sind keine Spiegelvölker gewährleistet.
		// Wenn erst nach der Vergabe der Völker und der VölkerRatings die Sortierung stattfindet, könnten die Spiegelvölker im selben Team landen.
		Collections.sort(players, new Comparator<EntityPlayer>() {
		    @Override
		    public int compare(EntityPlayer first, EntityPlayer second) {
		        return second.getEntireRating() - first.getEntireRating();
		    }
		});
		shuffleCivs(civs, isDoubleNation, isMirror);
	}
	
	private void shuffleCivs(ArrayList<EntityCiv> civs, boolean isDoubleNation, boolean isMirror){
		EntityCiv pickedCiv;
		for(int i=0;i<players.size();i++){
			random = randomNumber.nextInt(civs.size());
			pickedCiv = civs.get(random);
//	    	players.get(i).setCivRating(civs.get(random).getCivRating(players.get(i).getId()));
	    	players.get(i).setPickedCiv(pickedCiv);
	    	
	    	if(isMirror){
	    		i++;
				if(i<players.size()){
//			    	players.get(i).setCivRating(civs.get(random).getCivRating(players.get(i).getId()));
			    	players.get(i).setPickedCiv(pickedCiv);
				}
	    	}
	    	if(!isDoubleNation){
	    		civs.remove(random);
	    	}
		}
	}
	
	public void writeMatchFullRandom(Team teamA, Team teamB){
		
		int playersCount = players.size();
		int randomPlayer = 7;
		int oddPlayer_1 = 3, oddPlayer_2=4;
		
		switch(playersCount){
		case 3:
			oddPlayer_1 = 3; // Der stärkste Spieler spielt alleine gegen die zwei Schwächsten
			break;
		case 5:
			oddPlayer_1 = 1; // Spieler 1 und Spieler 2 spielen im selben Team
			oddPlayer_2 = 5; //Deswegen 5, damit oddPlayer_2 nicht im Team A landet.
			break;
		case 8:
			randomPlayer = randomNumber.nextInt(2)+6; //(0 bis 1)+7 Entweder 6 oder 7.
			break;
		}
		
		for(int i=0;i<players.size();i++){
			if(i==0||i==oddPlayer_1||i==oddPlayer_2||i==randomPlayer){
				teamA.addPlayerToTeam(players.get(i));
			}
			else{
				teamB.addPlayerToTeam(players.get(i));
			}
		}
		teamCalculation(teamA, teamB);
	}
	
	public void writeMatchHalfwayRandom(Team teamA, Team teamB){
		teamCalculation(teamA, teamB);
	}
	
	private void teamCalculation(Team teamA, Team teamB){
		teamA.setRatings();
		teamB.setRatings();
		teamA.calcTeamFav(teamA.getTeamEntireRating(), teamB.getTeamEntireRating());
		teamB.calcTeamFav(teamB.getTeamEntireRating(), teamA.getTeamEntireRating());
		teamA.calcTeamCivFav(teamA.getTeamCivRating(), teamB.getTeamCivRating());
		teamB.calcTeamCivFav(teamB.getTeamCivRating(), teamA.getTeamCivRating());
	}
}
