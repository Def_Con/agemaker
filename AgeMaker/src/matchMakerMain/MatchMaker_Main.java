package matchMakerMain;

import java.util.prefs.Preferences;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import language.Language;


public class MatchMaker_Main extends Application{

	private static final String version = "4.1_28.08.2018";

	@Override
	public void start(Stage stage) throws Exception {
		Preferences pref = Preferences.userNodeForPackage(MatchMaker_Main.class);
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/matchMakerView/MatchMaker_MainScene.fxml"), new Language(pref.get("Language","de")).getBundle());
		
		Parent root = loader.load();
		Scene scene = new Scene(root);
		
		stage.setScene(scene);
		stage.setTitle("AgeMaker "+version+"@Defcon85");
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
