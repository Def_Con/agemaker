package sysprint;

import database.Team;

public class PrintTeamData {

	public void printTeam(Team team, String teamName, String dateTime) {
		echo("\n");
		echo("//--------------------------------------------------------------//");
		echo("//--------------------------" + teamName + "------------------------------//");
		echof("Datum:", dateTime);
		echof("Karte:", team.getTeam().get(0).getPickedMap().getName());
		echof("ID:", team.getTeam().get(0).getPickedMap().getId());
		for (int i = 0; i < team.getTeam().size(); i++) {
			echof(" ", " ");
			echof("Spieler:", team.getTeam().get(i).getName());
			echof("Spieler-ID:", team.getTeam().get(i).getId());
			echof("Spieler-Rating:", team.getTeam().get(i).getPlayerRating());
			echof("Spieler-Neues Rating:", (team.getTeam().get(i).getPlayerRating() + team.getPlayerPoints()));
			echof("Civ:", team.getTeam().get(i).getPickedCiv().getName());
			echof("Civ-ID:", team.getTeam().get(i).getPickedCiv().getId());
			echof("Civ-Rating:", team.getTeam().get(i).getCivRating());
			echof("Civ-Neues Rating:", team.getTeam().get(i).getCivRating() + team.getCivPoints());
			echof("Map-Rating:", team.getTeam().get(i).getMapRating());
			echof("Map-Neues Rating:", team.getTeam().get(i).getMapRating() + team.getMapPoints());
			echof("GesamtRating:", team.getTeam().get(i).getEntireRating());
		}
		echo("\n");
		echo("Gesamtes Teamrating:    " + team.getTeamRating());
		echo("Gesamtes Karatenrating: " + team.getTeamMapRating());
		echo("Gesamtes Volkrating:    " + team.getTeamCivRating());
		echo("Gesamtes Rating:        " + team.getTeamEntireRating());
		echo("Team Fav:               " + team.isTeamFav());
		echo("Nation Fav:             " + team.isTeamCivFav());
		echo("//--------------------------------------------------------------//");
		echo("\n");
	}

	private void echo(String text) {
		System.out.println(text);
	}

	private void echof(String textTitle, String value) {
		System.out.print("//");
		System.out.printf("\t\t%-24s%-10s\t\t", textTitle, value);
		System.out.print("//");
		System.out.println();
	}

	private void echof(String textTitle, int value) {
		System.out.print("//");
		System.out.printf("\t\t%-24s%-10d\t\t", textTitle, value);
		System.out.print("//");
		System.out.println();
	}
}
